<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExerciseInstance extends Model
{
    protected $fillable = [
        'day_id', 'exercise_id', 'exercise_duration', 'order',
    ];
    public $timestamps = false;

    public function exercice(){
        return $this->belongsTo('App\Exercise');
    }

    public function day(){
        return $this->belongsTo('App\PlanDay', 'day_id');
    }
}
