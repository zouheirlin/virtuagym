<?php

namespace App\Http\Controllers;

use App\Repositories\PlanRepository;
use Illuminate\Http\Request;
use App\ExerciseInstance;
use App\Jobs\EmailJob;
use App\Plan;
use App\User;

class PlanController extends Controller
{
    protected $plan;

    public function __construct(PlanRepository $plan){
       $this->plan = $plan;
    }

    public function getPlans(){
        $plans = $this->plan->with('days','days.exercises','users')->all();
        return response()->json(['plans' => $plans]);
    }

    public function createPlan(Request $request){
        $plan = $this->plan->create($request->all());
        $plan['days'] = [];
        $plan['users'] = [];
        return response()->json(['plan' => $plan, 'message' => 'Plan added successfully']);
    }

    public function updatePlan(Request $request){
        $plan = $this->plan->find($request->id);
        $changes = false;
        if($plan){
            if($plan->plan_name != $request->plan_name){
                $plan->plan_name = $request->plan_name;
                $changes = true;
            }
            if($plan->plan_description != $request->plan_description){
                $plan->plan_description = $request->plan_description;
                $changes = true;
            }
            if($plan->plan_difficulty != $request->plan_difficulty){
                $plan->plan_difficulty = $request->plan_difficulty;
                $changes = true;
            }
            if($changes){
                $plan->save();
                foreach($plan->users as $user){
                    $content = $plan->plan_name ." that you are assigned to is being updated.";
                    EmailJob::dispatch($user, $content);
                }
                return response()->json(['message' => 'Plan updated successfully']);
            }
            return;
        }else{
            return response()->json(['message' => 'Plan not found']);
        }
    }

    public function deletePlan(Request $request){
        $plan = $this->plan->find($request->id);
        if($plan){
            foreach($plan->users as $user){
                $content = $plan->plan_name ." that you are assigned to is being deleted.";
                EmailJob::dispatch($user, $content);
            }
            $plan->delete();
            return response()->json(['message' => 'Plan deleted successfully']);
        }else{
            return response()->json(['message' => 'Plan not found']);
        }
    }

}
