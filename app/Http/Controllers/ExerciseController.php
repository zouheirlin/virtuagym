<?php

namespace App\Http\Controllers;

use App\Repositories\ExerciseRepository;
use App\Repositories\DayRepository;
use App\Repositories\ExerciseInstanceRepository;
use Illuminate\Http\Request;
use App\PlanDay;
use App\Exercise;
use App\ExerciseInstance;

class ExerciseController extends Controller
{
    protected $exercise;
    protected $exerciseIn;
    protected $day;

    public function __construct(ExerciseRepository $exercise, ExerciseInstanceRepository $exerciseIn, DayRepository $day){
       $this->exercise = $exercise;
       $this->exerciseIn = $exerciseIn;
       $this->day = $day;
    }

    public function getExercises(){
        $exercises = $this->exercise->all();
        return response()->json(['exercises' => $exercises]);
    }

    public function addDayExercise(Request $request){
        $count = $this->day->find($request->day)->exercises->count();
        $data = [
            'day_id' => $request->day,
            'exercise_id' => $request->exercise['id'],
            'exercise_duration' => $request->exercise['duration'],
            'order' => ++$count
        ];
        $exercise = $this->exerciseIn->create($data);

        return response()->json(['exercise' => $exercise, 'message' => 'Exercise added successfully']);
    }

    public function updateExerciseOrder(Request $request){
        $direction = $request->direction;
        $day = $this->day->find($request->day);
        $exercises = $day->exercises;
        $ex = $this->exerciseIn->find($request->exercise);
        $no = 0;
        if($direction == "up"){
            $no = $ex->order - 1;
        }else{
            $no = $ex->order + 1;
        }
        foreach($exercises as $exercise){
            if($exercise->order == $no){
                $exercise->order = $ex->order;
                $ex->order = $no;
                $exercise->save();
                $ex->save();
                return response()->json(['e1' => $ex,'e2' => $exercise ,'day' => $day->id,'plan' => $day->plan_id,'message' => 'Order changed']);
            }
        }  
    }

    public function removeDayExercise(Request $request){
        $this->exerciseIn->delete($request->id);
        return response()->json(['message' => 'Exercise removed successfully']);
    }

    
}
