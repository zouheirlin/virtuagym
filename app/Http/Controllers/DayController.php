<?php

namespace App\Http\Controllers;

use App\Repositories\DayRepository;
use App\Repositories\PlanRepository;
use Illuminate\Http\Request;
use App\PlanDay;
use App\Plan;

class DayController extends Controller
{
    protected $day;
    protected $plan;

    public function __construct(DayRepository $day, PlanRepository $plan){
       $this->day = $day;
       $this->plan = $plan;
    }

    public function addDay(Request $request){
        
        $count = $this->plan->find($request->plan)->days->count();
        $data = [
            'plan_id' => $request->plan,
            'day_name' => $request->day,
            'order' => ++$count
        ];
        $day = $this->day->create($data);
        $day['exercises'] = [];
        
        return response()->json(['day' => $day, 'message' => 'Day added successfully']);
    } 

    public function updateDay(Request $request){
        $day = $this->day->update(['day_name' => $request->day_name],$request->id);
        return response()->json(['message' => 'Day name updated successfully']);
    }

    public function updateDayOrder(Request $request){

    }

    public function removeDay(Request $request){
        $day = $this->day->find($request->id);
        if($day->exercises->count() > 0){
            foreach($day->exercises as $exercise){
                $exercise->delete();
            }
        }
        $day->delete();
        return response()->json(['message' => 'Day removed successfully']);
    }
}
