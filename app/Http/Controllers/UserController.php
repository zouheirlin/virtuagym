<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Jobs\EmailJob;
use App\User;
use App\Plan;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserRepository $user){
       $this->user = $user;
    }

    public function getUsers(){
        $users = $this->user->all();
        return response()->json(['users' => $users]);
    }

    public function addUser(Request $request){
        $user = $this->user->create($request->all());
        return response()->json(['user' => $user, 'message' => 'User added successfully']);
    }

    public function updateUser(Request $request){
        $this->user->update($request->all(),$request->id);
        return response()->json(['message' => 'User updated successfully']);
    }

    public function removeUser(Request $request){
        $this->user->delete($request->id);
        return response()->json(['message' => 'User deleted successfully']);
    }

    public function addPlanUser(Request $request){
        $user = $this->user->find($request->user['id']);
        if(!$user->plan->contains($request->plan)){
            $user->plan()->attach($request->plan);
            $content = "You are being assigned to a new plan";
            EmailJob::dispatch($user, $content);
        }
        return response()->json(['message' => 'Users assigned to your plan']);
    }
    
    public function removePlanUser(Request $request){
        $user = $this->user->find($request->user);
        $user->plan()->detach($request->plan);
        $content = "You are being unassigned from your plan";
        EmailJob::dispatch($user, $content);
        return response()->json(['message' => 'User detached from your plan']);
    }

}
