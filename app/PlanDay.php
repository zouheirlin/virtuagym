<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanDay extends Model
{

    protected $fillable = [
        'plan_id','day_name', 'order',
    ];
    
    public $timestamps = false;
    
    public function plan(){
        return $this->belongsTo('App\Plan');
    }

    public function exercises(){
        return $this->hasMany('App\ExerciseInstance', 'day_id');
    }
}
