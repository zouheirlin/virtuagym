<?php 
namespace App\Repositories;

use App\Repositories\Repository;

class ExerciseInstanceRepository extends Repository {

    function model()
    {
        return 'App\ExerciseInstance';
    }

}