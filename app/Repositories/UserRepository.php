<?php 
namespace App\Repositories;

use App\Repositories\Repository;

class UserRepository extends Repository {

    function model()
    {
        return 'App\User';
    }

}