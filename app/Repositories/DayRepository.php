<?php 
namespace App\Repositories;

use App\Repositories\Repository;

class DayRepository extends Repository {

    function model()
    {
        return 'App\PlanDay';
    }

}