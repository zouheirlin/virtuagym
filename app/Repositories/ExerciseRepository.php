<?php 
namespace App\Repositories;

use App\Repositories\Repository;

class ExerciseRepository extends Repository {

    function model()
    {
        return 'App\Exercise';
    }

}