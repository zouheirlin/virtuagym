<?php 
namespace App\Repositories;

use App\Repositories\Repository;

class PlanRepository extends Repository {

    function model()
    {
        return 'App\Plan';
    }

}