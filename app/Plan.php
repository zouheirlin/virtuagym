<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    use SoftDeletes;
    public $timestamps = false;
    
    protected $fillable = [
        'plan_name', 'plan_description', 'plan_difficulty',
    ];
    
    public function users(){
        return $this->belongsToMany('App\User', 'plan_users', 'plan_id', 'user_id');
    }

    public function days(){
        return $this->hasMany('App\PlanDay');
    }
    
}
