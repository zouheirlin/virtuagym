<?php

use Illuminate\Database\Seeder;

class ExerciseInstanceSeed extends Seeder
{
    public function run()
    {
        DB::table('exercise_instances')->insert([
            [
                'exercise_id' => 5,
                'day_id' => 1,
                'exercise_duration' => 300,
                'order' => 1
            ],
            [
                'exercise_id' => 6,
                'day_id' => 1,
                'exercise_duration' => 900,
                'order' => 3
            ],
            [
                'exercise_id' => 7,
                'day_id' => 1,
                'exercise_duration' => 900,
                'order' => 2
            ],
            [
                'exercise_id' => 1,
                'day_id' => 2,
                'exercise_duration' => 150,
                'order' => 1
            ],
            [
                'exercise_id' => 2,
                'day_id' => 2,
                'exercise_duration' => 300,
                'order' => 2
            ],
            [
                'exercise_id' => 3,
                'day_id' => 2,
                'exercise_duration' => 300,
                'order' => 3
            ],
            [
                'exercise_id' => 4,
                'day_id' => 2,
                'exercise_duration' => 500,
                'order' => 4
            ],
        ]);
    }
}
