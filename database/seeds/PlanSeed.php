<?php

use Illuminate\Database\Seeder;

class PlanSeed extends Seeder
{
    public function run()
    {
        DB::table('plans')->insert([
            [
                'plan_name' => 'First plan',
                'plan_description' => 'Just a dummy :-)',
                'plan_difficulty' => 2
            ]
        ]);
    }
}
