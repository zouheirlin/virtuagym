<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(ExerciseSeed::class);
        $this->call(PlanSeed::class);
        $this->call(PlanDaySeed::class);
        $this->call(ExerciseInstanceSeed::class);
        $this->call(UserSeed::class);
    }
}
