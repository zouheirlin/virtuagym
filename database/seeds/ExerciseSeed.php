<?php

use Illuminate\Database\Seeder;

class ExerciseSeed extends Seeder
{
    public function run()
    {
        DB::table('exercises')->insert([
            [
                'exercise_name' => 'Crunch',
            ],
            [
                'exercise_name' => 'Air squat',
            ],
            [
                'exercise_name' => 'Windmill',
            ],
            [
                'exercise_name' => 'Push-up',
            ],
            [
                'exercise_name' => 'Rowing Machine',
            ],
            [
                'exercise_name' => 'Walking',
            ],
            [
                'exercise_name' => 'Running',
            ]
        ]);

    }
}
