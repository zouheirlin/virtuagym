<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            [
                'firstname' => 'Zouheir',
                'lastname' => 'Layine',
                'email' => 'test1@gmail.com',
                'password' => 'none'
            ],
            [
                'firstname' => 'Mark',
                'lastname' => 'User',
                'email' => 'test2@gmail.com',
                'password' => 'none'
            ],
        ]);
    }
}
