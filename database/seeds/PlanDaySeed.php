<?php

use Illuminate\Database\Seeder;

class PlanDaySeed extends Seeder
{

    public function run()
    {
        DB::table('plan_days')->insert([
            [
                'plan_id' => 1,
                'day_name' => 'Day 1 - Cardio',
                'order' => 1
            ],
            [
                'plan_id' => 1,
                'day_name' => 'Day 2 - Other exercises',
                'order' => 2
            ]
        ]);
    }
}
