**Virtuagym app** 

Virtuagym application for assignment, view demo [here](https://noreferer.pro/)

To deploy, clone the project and run these commands in project's dir: 

`php artisan migrate`

`php artisan db:seed`

`php artisan serve`
