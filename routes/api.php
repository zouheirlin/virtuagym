<?php

use Illuminate\Http\Request;

Route::get('/plans', 'PlanController@getPlans');
Route::post('/plans', 'PlanController@createPlan');
Route::put('/plans', 'PlanController@updatePlan');
Route::delete('/plans', 'PlanController@deletePlan');

Route::post('/plans/users', 'UserController@addPlanUser');
Route::delete('/plans/users', 'UserController@removePlanUser');

Route::post('/plans/days', 'DayController@addDay');
Route::put('/plans/days', 'DayController@updateDay');
Route::delete('/plans/days', 'DayController@removeDay');

Route::get('/exercises', 'ExerciseController@getExercises');
Route::post('/plans/days/exercises', 'ExerciseController@addDayExercise');
Route::put('/plans/days/exercises/order', 'ExerciseController@updateExerciseOrder');
Route::delete('/plans/days/exercises', 'ExerciseController@removeDayExercise');

Route::get('/users', 'UserController@getUsers');
Route::post('/users', 'UserController@addUser');
Route::put('/users', 'UserController@updateUser');
Route::delete('/users', 'UserController@removeUser');

