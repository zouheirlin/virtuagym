export default {
    namespaced: true,
    state: {
        exercises: [],

    },
    getters: {
        exercises: state => state.exercises,
    },
    mutations: {
        UPDATE_EXERCISES(state, payload){
            state.exercises = payload;
        },
    },
    actions: {
        fetchExercises({commit}){
            axios.get('/api/exercises')
            .then((res) => {
                commit('UPDATE_EXERCISES', res.data.exercises);
            });
        },
    }
}