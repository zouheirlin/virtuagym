import Vue from 'vue'
import Vuex from 'vuex'

import plans from './plans';
import exercises from './exercises';
import users from './users';

Vue.use(Vuex)

export default new Vuex.Store({
     modules: {
	     plans,
          exercises,
          users,
     },
});
