import { resolve } from "path";

export default {
    namespaced: true,
    state: {
        plans: [],
    },
    getters: {
        plans: state => state.plans,
        search: state => id => {
            if(state.plans.length > 0)
                return state.plans.filter(p => p.id == id)[0]
            else 
                return ''
        },
    },
    mutations: {
        ADD_PLAN(state, payload){
            state.plans.push(payload);
        },
        ADD_PLANDAYS(state, day){
            state.plans = state.plans.reduce((plans, p) => {
                if(p.id == day.plan_id){
                    p.days.push(day);
                }
                return  [...plans, p]
            }, []);
        },
        ADD_DAYEXERCISE(state, exercise){
            state.plans.forEach(p => {
                p.days = p.days.filter(d => {
                    if(d.id == exercise.day_id){
                        d.exercises.push(exercise);
                    }
                    return true;
                })
            });
        },
        UPDATE_PLANS(state, payload){
            state.plans = payload;
        },
        UPDATE_PLAN(state, plan){
            state.plans.forEach(p => {
                if(p.id == plan.id){
                    p.plan_name = plan.plan_name;
                    p.plan_description = plan.plan_description;
                    p.plan_difficulty = plan.plan_difficulty;
                }
            })
        },
        UPDATE_PLAN_NAME(state, data){
            state.plans.forEach(p => {
                if(p.id == data.id){
                    p.plan_name = data.value;
                }
            })
        },
        UPDATE_PLAN_DESC(state, data){
            state.plans.forEach(p => {
                if(p.id == data.id){
                    p.plan_description = data.value;
                }
            })
        },
        UPDATE_PLAN_DIFF(state, data){
            state.plans.forEach(p => {
                if(p.id == data.id){
                    p.plan_difficulty = data.value;
                }
            })
        },
        UPDATE_USERS(state, data){
            state.plans.forEach(p => {
                if(p.id == data.plan){
                    p.users.push(data.user);
                }
            });
        },
        UPDATE_DAYNAME(state, data){
            state.plans.forEach(p => {
                if(p.id == data.plan){
                    p.days = p.days.filter(d => {
                        if(d.id == data.id){
                            d.day_name = data.day_name;
                        }
                        return true;
                    })
                }
            });
        },
        UPDATE_EXERCISE_ORDER(state, payload){
            state.plans.forEach(p => {
                if(p.id == payload.plan){
                    p.days.forEach(d => {
                        if(d.id == payload.day){
                            let e1 = d.exercises.find( e => e.id == payload.e1.id);
                            let e2 = d.exercises.find( e => e.id == payload.e2.id);
                            e1.order = payload.e1.order;
                            e2.order = payload.e2.order;
                            return;
                        }
                    });
                }
            });
        },
        REMOVE_PLAN(state, id){
            state.plans = state.plans.filter(p => p.id != id);
        },
        REMOVE_EXERCISE(state, id){
            state.plans.forEach(p => {
                p.days.forEach(d => {
                    d.exercises = d.exercises.filter(e => e.id != id);
                })
            });
        },
        REMOVE_DAY(state, id){
            state.plans.forEach(p => {
                p.days = p.days.filter(d => d.id != id);
            });
        },
        REMOVE_PLANUSER(state, data){
            state.plans.forEach(p => {
                if(p.id == data.plan){
                    p.users = p.users.filter(u => u.id != data.user);
                }
            });
        }
    },
    actions: {
        fetchPlans({commit}){
            axios.get('/api/plans')
            .then((res) => {
                commit('UPDATE_PLANS', res.data.plans);
            });
        },
        addPlan({commit}, plan){
            return new Promise((resolve,reject) => {
                axios.post('/api/plans', plan)
                .then((res) => {
                    commit('ADD_PLAN', res.data.plan);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
        },
        addPlanUser({commit}, data){
            return new Promise((resolve,reject) => {
                axios.post('/api/plans/users', data)
                .then((res) => {
                    commit('UPDATE_USERS', data);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
        },
        addPlanDay({commit}, data){
            return new Promise((resolve,reject) => {
                axios.post('/api/plans/days', data)
                .then((res) => {
                    commit('ADD_PLANDAYS', res.data.day);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
            
        },
        addDayExercise({commit, state}, data){
            return new Promise((resolve,reject) => {
                if(state.plans.filter(p => {
                    if(p.id == data.plan){
                        return p.days.filter(d => {
                            if(d.id == data.day){
                                return d.exercises.filter(e => e.exercise_id == data.exercise.id).length > 0;
                            }
                            return false;
                        }).length > 0;
                    }
                    return false;
                }).length > 0) {
                    reject('Exercise already exists');
                    return;
                };
                axios.post('/api/plans/days/exercises', data)
                .then((res) => {
                    commit('ADD_DAYEXERCISE', res.data.exercise);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
        },
        updateDayName({commit}, data){
            return new Promise((resolve,reject) => {
                axios.put('/api/plans/days', data)
                .then((res) => {
                    commit('UPDATE_DAYNAME', data);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            })
        },
        updateExerciseOrder({commit}, data){
            return new Promise((resolve, reject) => {
                axios.put('/api/plans/days/exercises/order', data)
                .then((res) => {
                    commit('UPDATE_EXERCISE_ORDER', res.data);
                    resolve();
                })
            })
           
        },
        editPlan({commit}, plan){
            return new Promise((resolve,reject) => {
                axios.put('/api/plans', plan)
                .then((res) => {
                    commit('UPDATE_PLAN', plan);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
        },
        removeExercise({commit}, id){
            return new Promise((resolve,reject) => {
                axios.delete('/api/plans/days/exercises', {data: {id: id} })
                .then((res) => {
                    commit('REMOVE_EXERCISE', id);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            })
        },
        removeDay({commit}, id){
            return new Promise((resolve,reject) => {
                axios.delete('/api/plans/days', {data: {id: id} })
                .then((res) => {
                    commit('REMOVE_DAY', id);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            })
        },
        removePlanUser({commit}, data){
            return new Promise((resolve,reject) => {
                axios.delete('/api/plans/users', {data: data})
                .then((res) => {
                    commit('REMOVE_PLANUSER', data);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
        },
        removePlan({commit}, id){
            return new Promise((resolve,reject) => {
                axios.delete('/api/plans', {data: {id: id} })
                .then((res) => {
                    commit('REMOVE_PLAN', id);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            })
        },


    }
}