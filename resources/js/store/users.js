export default {
    namespaced: true,
    state: {
        users: [],
    },
    getters: {
        users: state => state.users,
    },
    mutations: {
        UPDATE_USERS(state, payload){
            state.users = payload;
        },
        ADD_USER(state, user){
            state.users.push(user);
        },
        UPDATE_USER(state, user){
            state.users.filter(u => {
                if(u.id == user.id){
                    u = user;
                }
                return true;
            });
        },
        REMOVE_USER(state, id){
            state.users = state.users.filter(u => u.id != id);
        }
    },
    actions: {
        fetchUsers({commit}){
            axios.get('/api/users')
            .then((res) => {
                commit('UPDATE_USERS', res.data.users);
            });
        },
        addUser({commit}, user){
            return new Promise((resolve,reject) => {
                axios.post('/api/users', user)
                .then((res) => {
                    commit('ADD_USER', res.data.user);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
        },
        editUser({commit}, user){
            return new Promise((resolve,reject) => {
                axios.put('/api/users', user)
                .then((res) => {
                    commit('UPDATE_USER', user);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
        },
        removeUser({commit}, id){
            return new Promise((resolve,reject) => {
                axios.delete('/api/users', {data: {id: id}})
                .then((res) => {
                    commit('REMOVE_USER', id);
                    resolve(res);
                })
                .catch((err) => {
                    reject(err);
                });
            });
        }
    }
}