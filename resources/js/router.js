import Vue from 'vue'
import Router from 'vue-router'
import MainApp from './layouts/MainApp.vue';
import PlanDetails from './views/PlanDetails.vue';
import Users from './views/Users.vue';
import Plans from './views/Plans.vue';
import Error404 from './views/Error404.vue';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
		return { x: 0, y: 0 }
	},
    routes: [
        {
            path: '/',
            name:'home',
            component: MainApp,
            children: [
                {
                    path: '/plans',
                    name: 'plans',
                    component: Plans,
                },
                {
                    path: '/plans/:id',
                    name: 'details',
                    component: PlanDetails,
                },
                {
                    path: '/users',
                    name: 'users',
                    component: Users,
                },
                {
                    path: '/error-404',
                    component: Error404
                }
            ]
        },
        {
            path: '*',
            redirect: '/error-404'
        }
    ]
});

export default router