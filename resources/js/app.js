require('./bootstrap');
import Vue from 'vue';
import App from './App.vue'
import router from './router';
import store from './store/store';
import VueSweetalert2 from 'vue-sweetalert2';
import Notifications from 'vue-notification'

require('./icons');

Vue.use(VueSweetalert2);
Vue.use(Notifications);

Vue.config.productionTip = false

const app = new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');

