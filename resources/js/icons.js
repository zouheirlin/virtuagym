import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPen, faPlus, faTimes, faTrash, faPlusCircle, faStopwatch, faSave, faSort, faSortUp, faSortDown, faCaretSquareDown, faUsers, faUserPlus, faEllipsisV, faArrowAltCircleRight, faArrowAltCircleLeft, faArrowsAltV} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faPen, faPlus, faTimes, faTrash, faPlusCircle, faStopwatch, faSave, faSort, faSortUp, faSortDown, faCaretSquareDown, faUsers, faUserPlus, faEllipsisV, faArrowAltCircleRight, faArrowAltCircleLeft, faArrowsAltV);

Vue.component('font-awesome-icon', FontAwesomeIcon);