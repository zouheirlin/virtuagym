window._ = require('lodash');
import NProgress from 'nprogress';
import 'nprogress/nprogress.css'

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.axios.interceptors.request.use(config => {
    NProgress.start()
    return config
});

window.axios.interceptors.response.use(
    function(response) { 
        NProgress.done()
        return response;
    }, 
    function(error) {
        NProgress.done();
        if( error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false ) {
            return Promise.reject(error);
        }
        if (error.response) {
            return Promise.reject(error.response.data.message);
        }
    }
);


