Hello <i>{{ $data->receiver }}</i>,

<p>This is an email to update you about your plans</p>
 
<p>{{$data->content}}</p>

Best regards,
<br/>
<i>{{ $data->sender }}</i>