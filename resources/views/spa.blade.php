<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>VirtuaGym App</title>
        <script>
            window.Laravel = {!! json_encode([
                'url' => url('/'),
            ]) !!}
        </script>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('css/iconfont.css')}}">
    </head>
    <body>
        <div id="app"></div>

        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
